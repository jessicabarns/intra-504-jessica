package ca.qc.claurendeau;

import java.lang.reflect.Array;

import ca.qc.claurendeau.exceptions.DataException;
import ca.qc.claurendeau.exceptions.DatabaseException;
import ca.qc.claurendeau.exceptions.NetworkException;

public class DataClient {
	DataServer dataServer;

	public DataClient(DataServer dataServer) {
		this.dataServer = dataServer;
	}

	public int processData(int entryID) {
		int result = -1;
		try {
			dataServer.initiateNewTransaction(entryID);
			
			String data = dataServer.getCSVData();
			int output = getCSVInputLeft(data) + getCSVInputRight(data);
			int coefficient = getCSVCoefficient(data);
			
			boolean sendVoltage = dataServer.okToSendVoltage(output, coefficient);
			if(sendVoltage){
				dataServer.sendVoltage(output, coefficient);
				result = output * coefficient;
			}
			
			dataServer.finalizeTransaction();
		} catch (DatabaseException e) {
			e.printStackTrace();
		} catch (DataException e) {
			e.printStackTrace();
		} catch (NetworkException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private String getCSVData(int position, String data) throws Exception{	
		String[] dataArray = data.split(",");
		if(dataArray.length <= position){
			throw new Exception("Il manque des donn�es dans le CSV");
		}
		return dataArray[position];
	}

	private int getCSVInputLeft(String data) throws NumberFormatException, Exception {
		return Integer.parseInt(getCSVData(1, data));
	}

	private int getCSVInputRight(String data) throws NumberFormatException, Exception {
		return Integer.parseInt(getCSVData(2, data));
	}

	private int getCSVCoefficient(String data) throws NumberFormatException, Exception {
		return Integer.parseInt(getCSVData(3, data));
	}

}
