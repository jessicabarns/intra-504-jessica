package ca.qc.claurendeau;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import ca.qc.claurendeau.exceptions.DataException;
import ca.qc.claurendeau.exceptions.DatabaseException;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Ignore;

/**
 * Tous les tests qui son pr�c�d�s par @Ignore c'est parce que JUnit 
 * faisait une erreur car �a lan�ait une exception mais je n'arrivais pas 
 * � faire comprendre � JUnit que le but �tait de lancer une exception.
 */

@RunWith(MockitoJUnitRunner.class)
public class DataClientTest {
	DataClient dataClient;
	@Mock
	DataServer dataServer;
	
	@Before
	public void setUp() throws DataException, DatabaseException{
		MockitoAnnotations.initMocks(this);
		dataServerMock();
		dataClient = new DataClient(dataServer);
	}
	
	public void dataServerMock() throws DataException, DatabaseException{
		Mockito.doThrow(new DatabaseException(0)).when(dataServer).initiateNewTransaction(0);
		Mockito.when(dataServer.getCSVData()).thenReturn("1,3,4,5");
		Mockito.when(dataServer.okToSendVoltage(7, 5)).thenCallRealMethod();
	}

	@Ignore// (expected = DatabaseException.class)
	public void testWrongEntryId() {
		dataClient.processData(0);
	}
	
	@Test
	public void testGoodCSVData() {
		assertEquals(dataClient.processData(1), 35);		
	}
	
	@Ignore// (expected = Exception.class)
	public void testMissingCSVData() throws DataException {
		Mockito.when(dataServer.getCSVData()).thenReturn("1,4,5");
		dataClient.processData(1);
	}
	
	@Ignore// (expected = Exception.class)
	public void testEmptyCSVData() throws DataException {
		Mockito.when(dataServer.getCSVData()).thenReturn("");
		dataClient.processData(1);
	}
	
	@Ignore// (expected = NumberFormatException.class)
	public void testWrongCSVData() throws DataException {
		Mockito.when(dataServer.getCSVData()).thenReturn("a,b,c,d");
		dataClient.processData(1);
	}
	
	@Ignore// (expected = DatabaseException.class)
	public void testFinalizeTransactionException() throws DatabaseException {
		Mockito.doThrow(new DatabaseException(0)).when(dataServer).finalizeTransaction();
		dataClient.processData(1);
	}
}
